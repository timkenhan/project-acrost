#!/bin/bash -e

TARGET_ROOTFS=/mnt/gentoo

STAGE3_TARBALL=../stage3/stage3-amd64-latest.tar.bz2
REPO_SOURCE=../portage
PORTAGE_CONFDIR=./portage_config
CHROOT_FILES=./chroot_files
PKGLIST=pkgs/pkgs-full.txt



# first, install the system files
# respectively: stage3 tarball, portage repo, portage config
tar -C ${TARGET_ROOTFS} --xattrs -p -xf ${STAGE3_TARBALL}
rsync --delete -r ${PORTAGE_CONFDIR}/* ${TARGET_ROOTFS}/etc/portage
# comment this for clean build
rsync --xattrs -a ${REPO_SOURCE} ${TARGET_ROOTFS}/usr
# within the repo are the distfiles and binpackages
# comment out this three commands above when you're resuming

# mounting pseudo-fs
mount -t tmpfs tmpfs ${TARGET_ROOTFS}/tmp
mount -t proc proc ${TARGET_ROOTFS}/proc
mount -R /dev ${TARGET_ROOTFS}/dev
mount -R /sys ${TARGET_ROOTFS}/sys

# these are temporary files
cp ${CHROOT_FILES}/* ${TARGET_ROOTFS}/tmp
cp ${PKGLIST} ${TARGET_ROOTFS}/tmp/pkgs.txt
chmod +x ${TARGET_ROOTFS}/tmp/chroot_setup.sh
# uncomment below to enable networking during chroot when needed
# cp --remove-destination /etc/resolv.conf ${TARGET_ROOTFS}/etc

# running the rest in chroot environment
chroot ${TARGET_ROOTFS} /tmp/chroot_setup.sh

# unmounting pseudo-fs
umount -R ${TARGET_ROOTFS}/sys
umount -R ${TARGET_ROOTFS}/dev
umount ${TARGET_ROOTFS}/proc
umount ${TARGET_ROOTFS}/tmp



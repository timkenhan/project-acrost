#!/bin/bash -e

TIMEZONE=UTC
PORTAGE_PROFILE=default/linux/amd64/17.1/desktop
EMERGE_OPTION="--quiet"

PKGS=$(grep -v "^#" /tmp/pkgs.txt | grep ".*/.*")



# set timezone
echo $TIMEZONE > /etc/timezone

# generate ONLY selected locales
install /tmp/locale.gen /etc/

# uncomment for clean build
#emerge --sync

eselect profile set ${PORTAGE_PROFILE}

# possible circular dependencies here! for clean build, uncomment the line below
#USE="-tiff" emerge ${EMERGE_OPTION} --deep --update --newuse --with-bdeps=y @world
emerge ${EMERGE_OPTION} --deep --update --newuse --with-bdeps=y  @world

# possible circular dependencies here!
#USE="-libproxy" emerge ${EMERGE_OPTION} ${PKGS}
#emerge ${EMERGE_OPTION} --deep --update --newuse --with-bdeps=y  @world
# for clean build, uncomment the two lines above, and comment out the line below
emerge ${EMERGE_OPTION} ${PKGS}

emerge ${EMERGE_OPTION} --depclean

# add your daemons here!
rc-update add sysklogd default
rc-update add acpid default
#rc-update add NetworkManager default

ln -sf ../tmp/resolv.conf /etc/

install /tmp/resolvconf.conf /etc/

install /tmp/vimrc.local /etc/vim/

# feel free to comment these out if not using NetworkManager
rc-update add NetworkManager default
install -o polkitd /tmp/*.rules /etc/polkit-1/rules.d/
install /tmp/NetworkManager.conf /etc/NetworkManager/


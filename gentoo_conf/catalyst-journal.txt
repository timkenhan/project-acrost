First, catalyst was installed, no problem there.

Then the files are set up as well, seems easy.

Catalyst failed in trying to build stage1. 
This happened because symbolic links were used.
Turned out that the name extension of the link didn't match the original.
The catalyst looked thru the extension to determine decompression method.

Everything went well, until a circular dependency was encountered.
This was during the stage3 build. Turned out it's a known issue (bug #663440).
To get around this, "-filecaps" useflag was added.
Implemented by including a portage_confdir containing a package.use

Tried stage3 again without the portage_confdir.
Still won't build.

At least now a stage3 tarball is ready.

Turned out that the whole portage confdir was set up somewhere.
It was on the same site, except that it was poorly documented.
Now that the configuration is setup, all went without a hassle.

Onto stage4-minimal, everything was a breeze.
After untarring the build onto the disk image, it was ready to boot.
There isn't anything on the minimal, as the name states.
Also, the kernel provided didn't have driver for VM (particularly network)
